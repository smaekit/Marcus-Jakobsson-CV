//
//  JobTableViewCell.swift
//  Marcus Jakobsson CV
//
//  Created by Marcus Jakobsson on 2017-10-21.
//  Copyright © 2017 Marcus Jakobsson. All rights reserved.
//

import UIKit

class JobTableViewCell: UITableViewCell {

   
    @IBOutlet weak var jobLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
