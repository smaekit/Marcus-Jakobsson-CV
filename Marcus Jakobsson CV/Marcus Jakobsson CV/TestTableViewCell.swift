//
//  TestTableViewCell.swift
//  Marcus Jakobsson CV
//
//  Created by Marcus Jakobsson on 2017-10-19.
//  Copyright © 2017 Marcus Jakobsson. All rights reserved.
//

import UIKit

class TestTableViewCell: UITableViewCell {

  
    @IBOutlet weak var testTableView: UIImageView!
    
    @IBOutlet weak var phoneLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
