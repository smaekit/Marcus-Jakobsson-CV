//
//  MyTableViewController.swift
//  Marcus Jakobsson CV
//
//  Created by Marcus Jakobsson on 2017-10-18.
//  Copyright © 2017 Marcus Jakobsson. All rights reserved.
//

import UIKit

class MyTableViewController: UITableViewController {

    var row :Int?
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.backgroundColor = UIColor.init(red: 0.19, green: 0.22, blue: 0.28, alpha: 1.0)
        self.navigationController?.isNavigationBarHidden = true
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        switch section {
        case 0:
            return 5
        case 1:
            return 5
        default:
            return 1
        }
    }
   
    let workExperience:[Job] = [
        Job(companyName: "Jönköping University", imageName: "ju", description: "Datateknik - Mjukvaruutveckling och mobila plattformar \n08/2015 - Present"),
        Job(companyName: "Consid AB", imageName: "consid", description: "Student Ambassadör \n05/2017 - Present"),
        Job(companyName: "Flit AB", imageName: "flit", description: "Lager \n10/2015 - 10/2016"),
        Job(companyName: "Lernia AB", imageName: "lernia", description: "Truckförare \n04/2015 - 05/2015"),
        Job(companyName: "Volvo AB", imageName: "volvo", description: "Logistik \n10/2010 - 02/2015")
    ]
    
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "testCell", for: indexPath) as! TestTableViewCell
//        cell.backgroundColor = UIColor.init(red: 0.19, green: 0.22, blue: 0.28, alpha: 1.0)
//        return cell
//    }


    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Configure the cell...
        row = indexPath.row
        if indexPath.section == 0
        {
            switch indexPath.row
            {
            case 0:
                let cell = tableView.dequeueReusableCell(withIdentifier: "PhotoNameCell", for: indexPath) as! PhotoNameTableViewCell
                
                cell.myPhoto.image = UIImage(named: "CVphoto")!
                cell.name.text = "Marcus Jakobsson"
                return cell
            case 1:
                let cell = tableView.dequeueReusableCell(withIdentifier: "MailCell", for: indexPath) as! MailTableViewCell
                cell.contactInfo.text = "070-5409274"
                cell.icon.image = UIImage(named:"mobile")
                return cell
            case 2:
                let cell = tableView.dequeueReusableCell(withIdentifier: "MailCell", for: indexPath) as! MailTableViewCell
                cell.icon.image = UIImage(named:"mail")
                cell.contactInfo.text = "Marcus.Jakobsson@live.se"
                return cell
            case 3:
                let cell = tableView.dequeueReusableCell(withIdentifier: "MailCell", for: indexPath) as! MailTableViewCell
                cell.icon.image = UIImage(named:"pin")
                cell.contactInfo.text = "Apotekargränd 4"
                return cell
            case 4:
                let cell = tableView.dequeueReusableCell(withIdentifier: "MailCell", for: indexPath) as! MailTableViewCell
                cell.icon.image = UIImage(named:"linkedin")
                cell.contactInfo.text = "LinkedIn"
                return cell
            default:
                return UITableViewCell()
            }
        }
        else if indexPath.section == 1
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "JobCell", for: indexPath) as! JobTableViewCell
            cell.jobLabel.text = workExperience[row!].companyName
            cell.backgroundColor = UIColor.init(red: 0.19, green: 0.22, blue: 0.28, alpha: 1.0)
            return cell
        }
        else if indexPath.section == 2
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "JobCell", for: indexPath) as! JobTableViewCell
            cell.jobLabel.text = "Demo"
            cell.backgroundColor = UIColor.init(red: 0.19, green: 0.22, blue: 0.28, alpha: 1.0)
            return cell
        }
        return UITableViewCell()
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                return 150
            }
            else {
                return 60
            }
        default:
            return 60
        }
        
    }
    
    private func contactInfoHandler(contactInfo:String, handler: String) {
        
        if let contactInfoURL = URL(string: "\(handler)\(contactInfo)") {
            
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(contactInfoURL)) {
                application.open(contactInfoURL, options: [:], completionHandler: nil)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        row = indexPath.row
        switch indexPath.section {
        case 0:
            if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MailCell", for: indexPath) as! MailTableViewCell
                let number :String = cell.contactInfo.text!
                
                contactInfoHandler(contactInfo: number,handler: "tel://")
            }
            if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "MailCell", for: indexPath) as! MailTableViewCell
                let mailAdress :String = cell.contactInfo.text!
                contactInfoHandler(contactInfo: mailAdress, handler: "mailto:")
            }
            if indexPath.row == 3 {
                performSegue(withIdentifier: "AddressView", sender: self)
            }
            if indexPath.row == 4 {
                performSegue(withIdentifier: "WebPageView", sender: self)
            }
        case 1:
            performSegue(withIdentifier: "JobView", sender: self)
        case 2:
            performSegue(withIdentifier: "AnimationView", sender: self)
        default:
            performSegue(withIdentifier: "AnimationView", sender: self)
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "JobView"
        {
            if let destinationVC = segue.destination as? JobsViewController
            {
                destinationVC.companyName = workExperience[row!].companyName
                destinationVC.imageName = workExperience[row!].imageName
                destinationVC.jobDescription = workExperience[row!].description
                
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return ""
        case 1:
            return "Work experience"
        case 2:
            return "My skills"
        default:
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.textLabel?.font = UIFont(name: "System", size: 14.0)
        header.textLabel?.textAlignment = NSTextAlignment.center
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
