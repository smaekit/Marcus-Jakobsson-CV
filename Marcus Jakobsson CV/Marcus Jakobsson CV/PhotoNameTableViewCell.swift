//
//  PhotoNameTableViewCell.swift
//  Marcus Jakobsson CV
//
//  Created by Marcus Jakobsson on 2017-10-18.
//  Copyright © 2017 Marcus Jakobsson. All rights reserved.
//

import UIKit

class PhotoNameTableViewCell: UITableViewCell {

    @IBOutlet weak var myPhoto: UIImageView!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        myPhoto.layer.cornerRadius = myPhoto.frame.width/2
        myPhoto.layer.borderWidth = 4.0
        let myColor : UIColor = UIColor(hue: 0.5417, saturation: 1, brightness: 0.88, alpha: 1.0)
        myPhoto.layer.borderColor = myColor.cgColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
