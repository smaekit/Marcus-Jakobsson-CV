//
//  AnimationViewController.swift
//  Marcus Jakobsson CV
//
//  Created by Marcus Jakobsson on 2017-10-18.
//  Copyright © 2017 Marcus Jakobsson. All rights reserved.
//

import UIKit
import AVFoundation

class AnimationViewController: UIViewController {

    var audioPlayer: AVAudioPlayer = AVAudioPlayer()
    @IBOutlet weak var clownImage: UIImageView!
    
    @IBOutlet weak var message: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        message.text = "Happy Halloween!"
        message.alpha = 0
        clownImage.image = #imageLiteral(resourceName: "clown")
        clownImage.isHidden = true
        
        
        
    
        // Do any additional setup after loading the view.
    }

    override func viewDidAppear(_ animated: Bool) {
        let yeahFile = Bundle.main.path(forResource: "laugh", ofType: "wav")
        do{
            try audioPlayer = AVAudioPlayer(contentsOf: URL(fileURLWithPath: yeahFile!))
        }catch{
            print(error)
        }
        if true {
            sleep(2)
        }
        self.clownImage?.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5, animations: {() -> Void in
            self.clownImage.isHidden = false
            self.clownImage?.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 5.0, animations: {() -> Void in
                self.clownImage?.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
                self.clownImage.frame.origin.y += 700
                self.message.alpha = 1.0
            })
        })
        UIView.animate(withDuration: 5, animations: {() -> Void in
            self.message.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        })
        
        
        audioPlayer.play()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
