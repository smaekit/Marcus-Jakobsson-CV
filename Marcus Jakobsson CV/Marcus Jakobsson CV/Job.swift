//
//  Job.swift
//  Marcus Jakobsson CV
//
//  Created by Marcus Jakobsson on 2017-10-21.
//  Copyright © 2017 Marcus Jakobsson. All rights reserved.
//

import Foundation

struct Job {
    var companyName : String
    var imageName : String
    var description :String
}
