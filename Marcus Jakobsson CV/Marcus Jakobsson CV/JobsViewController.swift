//
//  JobsViewController.swift
//  Marcus Jakobsson CV
//
//  Created by Marcus Jakobsson on 2017-10-18.
//  Copyright © 2017 Marcus Jakobsson. All rights reserved.
//

import UIKit


class JobsViewController: UIViewController {

    var companyName :String?
    var imageName : String?
    var jobDescription: String?
    
    @IBOutlet weak var compDescription: UILabel!
    @IBOutlet weak var compImage: UIImageView!
    @IBOutlet weak var compName: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        compDescription.text = jobDescription
        compName.text = companyName
        compImage.image = UIImage(named: imageName!)
        
        // Do any additional setup after loading the view.
    }

    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
