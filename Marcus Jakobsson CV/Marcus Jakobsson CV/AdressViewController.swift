//
//  AdressViewController.swift
//  Marcus Jakobsson CV
//
//  Created by Marcus Jakobsson on 2017-10-18.
//  Copyright © 2017 Marcus Jakobsson. All rights reserved.
//

import UIKit
import MapKit

class AdressViewController: UIViewController {

    @IBOutlet weak var MapKit: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: 57.782208, longitude: 14.175642)
        MapKit.addAnnotation(annotation)
        
        let initialLocation = CLLocation(latitude: 57.782208, longitude: 14.175642)
        
        let regionRadius: CLLocationDistance = 1000
        func centerMapOnLocation(location: CLLocation) {
            let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                      regionRadius * 2.0, regionRadius * 2.0)
            MapKit.setRegion(coordinateRegion, animated: true)
            
        }
        centerMapOnLocation(location: initialLocation)
        // Do any additional setup after loading the view.
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
